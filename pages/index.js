import Layout from '../components/layout'
import TopAgents  from '../components/Table/TopAgents'
import TopProduct  from '../components/Table/TopProduct'
export default function Index() {
  return (
   <div>
    <Layout>
      <TopAgents />
      <TopProduct />
    </Layout>
  </div>
  )
}


