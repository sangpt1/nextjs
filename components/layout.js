import {Navbar} from './navbar'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
export default function Layout({ children }) {
  return (
    <div>
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Paper>
            <Navbar />
            <main>{children}</main>
        </Paper>
      </Grid>
    </Grid>
  </div>
  )
}