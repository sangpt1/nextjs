import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Đại Lý 1', 159, 6.0, 24, 4.0),
  createData('Đại lý 2', 237, 9.0, 37, 4.3),
  createData('Đại lý 3', 262, 16.0, 24, 6.0),
  createData('Đại lý 4', 305, 3.7, 67, 4.3),
  createData('Đại lý 5', 356, 16.0, 49, 3.9),
];

export default function Index() {
  return (
   <div>
      <TableContainer component={Paper}>
          <h5>Danh sach đại lý</h5>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Top đại lý</TableCell>
              <TableCell align="right">Calories</TableCell>
              <TableCell align="right">Fat&nbsp;(g)</TableCell>
              <TableCell align="right">Carbs&nbsp;(g)</TableCell>
              <TableCell align="right">Protein&nbsp;(g)</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                <Link href="/agency/dai-ly-a">
                  {row.name}
                </Link>
                </TableCell>
                <TableCell align="right">
                    <Link href="/agency/dai-ly-a">
                    <a> {row.calories} </a>
                    </Link>
                </TableCell>
                <TableCell align="right">{row.fat}</TableCell>
                <TableCell align="right">{row.carbs}</TableCell>
                <TableCell align="right">{row.protein}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
  </div>
  )
}


