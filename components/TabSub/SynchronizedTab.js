import React from 'react';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default function DistributionTab() {
  
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
 
  };

  return (
    <div>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Sản phẩm" {...a11yProps(0)} />
          <Tab label="Bài viết" {...a11yProps(1)} />
          <Tab label="Danh mục" {...a11yProps(2)} />
          <Tab label="Khuyến mãi" {...a11yProps(3)} />
          <Tab label="Chính sách" {...a11yProps(4)} />
          <Tab label="Từ khóa phạm vi" {...a11yProps(5)} />
          <Tab label="Cài đặt đồng bộ" {...a11yProps(6)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
          Sản phẩm
      </TabPanel>
      <TabPanel value={value} index={1}>
          Bài viết
      </TabPanel>
      <TabPanel value={value} index={2}>
          Danh mục
      </TabPanel>
      <TabPanel value={value} index={3}>
          Khuyến mãi
      </TabPanel>
      <TabPanel value={value} index={4}>
          Chính sách
      </TabPanel>
      <TabPanel value={value} index={5}>
          Từ khóa phạm vi
      </TabPanel>
      <TabPanel value={value} index={6}>
          Cài đặt đồng bộ
      </TabPanel>
    </div>
  );
  }