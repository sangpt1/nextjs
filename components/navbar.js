import React from 'react';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import { useRouter } from 'next/router'

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export function Navbar() {
  const router = useRouter()
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    if(newValue == 0) {
      router.push("/")
    }
    if(newValue == 1) {
      router.push("/agency")
    }
    if(newValue == 2) {
      router.push("/distribution")
    }
    if(newValue == 3) {
      router.push("/synchronized")
    }
    if(newValue == 4) {
      router.push("/report")
    }
    if(newValue == 5) {
      router.push("/setting")
    }
  };

  return (
    <div>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange}>
          <Tab label="Tổng quan" {...a11yProps(0)} />
          <Tab label="Đại Lý" {...a11yProps(1)} />
          <Tab label="Phân phối đơn hàng" {...a11yProps(3)} />
          <Tab label="Đồng bộ dữ liệu" {...a11yProps(4)} />
          <Tab label="Báo cáo" {...a11yProps(5)} />
          <Tab label="Cài đặt" {...a11yProps(6)} />
        </Tabs>
      </AppBar>
    </div>
  );
  }